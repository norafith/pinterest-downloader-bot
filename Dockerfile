FROM node:22-alpine3.19 AS build
COPY ./service /service
WORKDIR /service
RUN npm install
RUN apk add ffmpeg 
RUN apk add chromium
CMD node index.js
