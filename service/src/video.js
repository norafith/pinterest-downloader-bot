const puppeteer = require("puppeteer");
const exec = require("util").promisify(require("child_process").exec);

exports.getVideoM3u8Url = async (videoUrl, email, password) => {
  const browser = await puppeteer.launch({
    executablePath: "/usr/bin/chromium",
    args: ["--no-sandbox"]
  });
  const page = await browser.newPage();

  await page.goto(videoUrl, {
    waitUntil: ["domcontentloaded"],
  });

  if ((await page.locator('div[data-test-id="signup-button"]')) != null) {
    await page.goto("https://www.pinterest.com/login", {
      waitUntil: ["domcontentloaded"],
    });
    await page.locator("#email").fill(email);
    await page.locator("#password").fill(password);
    await page.locator('div[data-test-id="registerFormSubmitButton"]').click();
    await page.waitForNavigation();
    await page.goto(videoUrl);
  }

  responseUrls = [];
  page.on("response", (response) => {
    responseUrls.push(response.url());
  });
  await page.goto(videoUrl);

  downloadUrl = "";
  for (let url of responseUrls) {
    if (url.includes("m3u8")) {
      downloadUrl = url;
      break;
    }
  }

  await browser.close();
  return downloadUrl;
};

exports.getVideoFromM3u8 = async (url, savePath) => {
  let command = `ffmpeg -protocol_whitelist file,http,https,tcp,tls,crypto -i ${url} -c copy ${savePath}`;

  await exec(command).catch((e) => e);
};
