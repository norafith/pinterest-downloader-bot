const fs = require("fs");
const { Telegraf } = require("telegraf");
const { message } = require("telegraf/filters");
const video = require("./src/video");

const textMessageHandler = async (ctx) => {
  const full_url_regex = /https:\/\/(?:ru\.)?pinterest\.com\/pin\/\d+\//;
  const short_url_regex = /https:\/\/pin\.it\/[a-zA-Z0-9]+/;
  if (
    !ctx.message.text.match(full_url_regex) &&
    !ctx.message.text.match(short_url_regex)
  ) {
    ctx.reply(`"${ctx.message.text}" is not valid pinterest pin URL.`);
    return;
  }
  ctx.reply("Processing...");

  const email = process.env.EMAIL;
  const password = process.env.PASSWORD;
  let m3u8Url;
  try {
    m3u8Url = await video.getVideoM3u8Url(
      ctx.message.text,
      email,
      password
    );
  } 
  catch (e) {
    ctx.reply(`Got error ${e}.`);
    return;
  }
  if (!m3u8Url) {
    ctx.reply(`Couldn't find video with URL "${ctx.message.text}".`);
    return;
  }

  const savePath = `./videos/${ctx.update.update_id}.mp4`;
  ctx.reply("Downloading video... Please wait...");
  try {
    await video.getVideoFromM3u8(m3u8Url, savePath);
  }
  catch (e) {
    ctx.reply(`Got error ${e}.`);
    return;
  }

  ctx.reply("Sending video...");
  try {
    await ctx.replyWithVideo({
      source: fs.createReadStream(savePath),
    });
  }
  catch (e) {
    ctx.reply(`Got error ${e}.`);
    return;
  }

  fs.unlinkSync(savePath);
};

exports.startBot = () => {
  const token = process.env.BOT_TOKEN;
  // const token = "7300629591:AAFu0JniV-X1DlTvo6YqM_BI8gSlsUk5Vcc";
  const bot = new Telegraf(token);

  bot.start((ctx) => ctx.reply("Welcome!"));
  bot.help((ctx) => ctx.reply("Send me a link with pinterest video."));

  bot.on(message("text"), textMessageHandler);
  bot.launch();

  process.once("SIGINT", () => bot.stop("SIGINT"));
  process.once("SIGTERM", () => bot.stop("SIGTERM"));
};

exports.startBot();
