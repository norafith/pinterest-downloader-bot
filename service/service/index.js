const fs = require("fs");
const { Telegraf } = require("telegraf");
const { message } = require("telegraf/filters");
const video = require("./src/video");

const textMessageHandler = async (ctx) => {
  const full_url_regex = /https:\/\/(?:ru\.)?pinterest\.com\/pin\/\d+\//;
  const short_url_regex = /https:\/\/pin\.it\/[a-zA-Z0-9]+/;
  if (
    !ctx.message.text.match(full_url_regex) &&
    !ctx.message.text.match(short_url_regex)
  ) {
    ctx.reply(`"${ctx.message.text}" is not valid pinterest pin URL.`);
    return;
  }
  ctx.reply("Processing...");

  const m3u8Url = await video.getVideoM3u8Url(ctx.message.text);
  if (!m3u8Url) {
    ctx.reply(`Couldn't find video with URL "${ctx.message.text}".`);
    return;
  }

  const savePath = `./videos/${ctx.update.update_id}.mp4`;
  ctx.reply("Downloading video... Please wait...");
  await video.getVideoFromM3u8(m3u8Url, savePath);

  ctx.reply("Sending video...");
  await ctx.replyWithVideo({
    source: fs.createReadStream(savePath),
  });

  fs.unlinkSync(savePath);
};

exports.startBot = () => {
  // const token = process.env.BOT_TOKEN;
  const token = "7300629591:AAFu0JniV-X1DlTvo6YqM_BI8gSlsUk5Vcc";
  const bot = new Telegraf(token);

  bot.start((ctx) => ctx.reply("Welcome!"));
  bot.help((ctx) => ctx.reply("Send me a link with pinterest video."));

  bot.on(message("text"), textMessageHandler);
  bot.launch();

  process.once("SIGINT", () => bot.stop("SIGINT"));
  process.once("SIGTERM", () => bot.stop("SIGTERM"));
};

exports.startBot();
